
function validateData()
{
    if (document.theForm.name.value.length < 2)
    {
        document.getElementById("showErrorMessage1").innerHTML = "TOO SHORT: - should be at least 2 characters in length.";
        document.theForm.name.select();
        return false;
    }
    document.getElementById("showErrorMessage1").innerHTML = ""; //removes "old" error message




    if (document.theForm.name.value.charAt(0) < "A" || document.theForm.name.value.charAt(0) > "Z")
    {
        alert("The name must start with an UPPERCASE letter.");
        document.getElementById("showErrorMessage1").innerHTML = "Must start with uppercase letter";
        document.theForm.name.select();
        return false;
    }
    if (document.theForm.address.value.length < 10)
    {
        document.getElementById("showErrorMessage2").innerHTML = "Is not right form: - should be at least 10 characters in length.";
        document.theForm.address.select();
        return false;
    }










// if problem with COUNTY (SELECTION LIST)
    if (document.theForm.type.selectedIndex === 0)
    {
        alert("A type was not picked from the selection list.");
        document.theForm.type.focus();
        return false;
    }




    // check if NO preference radio button checked
    var radioNotChecked = true;
    for (var j = 0; j < document.theForm.preference.length; j++)
    {
        if (document.theForm.preference[j].checked)
        {
            radioNotChecked = false;
        }
    }

    if (radioNotChecked)
    {
        alert("Contact preference was not picked from the RADIO BUTTON set.");
        document.theForm.preference[0].focus();
        return false;
    }






// No validation errors
    writeLocalStorage();
    return true;

}

function writeLocalStorage()
{
    // WRITE TO LOCAL STORAGE, INSERTING "N/A" FOR 
    //		any NON-REQUIRED TEXTBOXES IF THEY ARE EMPTY

    localStorage.fullName = document.theForm.name.value;

    localStorage.address = document.theForm.address.value;

    if (document.theForm.age.value.length === 0)
    {
        localStorage.age = "N/A";
    }
    else
    {
        localStorage.age = document.theForm.age.value;
    }
    localStorage.type = document.theForm.type.value;
    localStorage.emailAddress = document.theForm.email.value;
    localStorage.contactPreference = document.theForm.preference.value;
    localStorage.rating = document.theForm.rate.value;
    localStorage.mobile = document.theForm.mobile.value;
    localStorage.comment = document.theForm.comment.value;
    localStorage.date = document.theForm.date.value;


}
